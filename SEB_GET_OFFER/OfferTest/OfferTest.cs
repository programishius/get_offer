﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SEB_GET_OFFER;

namespace OfferTest
{
    [TestClass]
    public class OfferTest
    {
        [TestMethod]
        public void GetProductTest()
        {
            GetOfferForm offer = new GetOfferForm();
            int? age = 18;
            int? income = 10000;
            bool student = false;

            Assert.AreEqual("Best offer is: Current Account", offer.GetProduct(age,income,student));
        }
    }
}
