﻿namespace SEB_GET_OFFER
{
    partial class GetOfferForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.getOfferBtn = new System.Windows.Forms.Button();
            this.ageInput = new System.Windows.Forms.TextBox();
            this.incomeInput = new System.Windows.Forms.TextBox();
            this.studentCheckbox = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // getOfferBtn
            // 
            this.getOfferBtn.Location = new System.Drawing.Point(12, 89);
            this.getOfferBtn.Name = "getOfferBtn";
            this.getOfferBtn.Size = new System.Drawing.Size(260, 23);
            this.getOfferBtn.TabIndex = 0;
            this.getOfferBtn.Text = "Get Offer";
            this.getOfferBtn.UseVisualStyleBackColor = true;
            this.getOfferBtn.Click += new System.EventHandler(this.getOfferBtn_Click);
            // 
            // ageInput
            // 
            this.ageInput.Location = new System.Drawing.Point(55, 10);
            this.ageInput.Name = "ageInput";
            this.ageInput.Size = new System.Drawing.Size(100, 20);
            this.ageInput.TabIndex = 1;
            // 
            // incomeInput
            // 
            this.incomeInput.Location = new System.Drawing.Point(55, 36);
            this.incomeInput.Name = "incomeInput";
            this.incomeInput.Size = new System.Drawing.Size(100, 20);
            this.incomeInput.TabIndex = 2;
            this.incomeInput.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numeric_input_KeyPress);
            // 
            // studentCheckbox
            // 
            this.studentCheckbox.AutoSize = true;
            this.studentCheckbox.Location = new System.Drawing.Point(5, 62);
            this.studentCheckbox.Name = "studentCheckbox";
            this.studentCheckbox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.studentCheckbox.Size = new System.Drawing.Size(63, 17);
            this.studentCheckbox.TabIndex = 3;
            this.studentCheckbox.Text = "Student";
            this.studentCheckbox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.studentCheckbox.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Age";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Income";
            // 
            // GetOfferForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 124);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.studentCheckbox);
            this.Controls.Add(this.incomeInput);
            this.Controls.Add(this.ageInput);
            this.Controls.Add(this.getOfferBtn);
            this.Name = "GetOfferForm";
            this.Text = "GET OFFER";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numeric_input_KeyPress);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button getOfferBtn;
        private System.Windows.Forms.TextBox ageInput;
        private System.Windows.Forms.TextBox incomeInput;
        private System.Windows.Forms.CheckBox studentCheckbox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

