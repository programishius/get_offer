﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace SEB_GET_OFFER
{
    public partial class GetOfferForm : Form
    {
        private ProductsList _products;
        private BundlesList _bundles;
        public GetOfferForm()
        {
            InitializeComponent();
            _products = GetProducts();
            _bundles = GetBundles();
        }
        private void numeric_input_KeyPress(object sender, KeyPressEventArgs e)
        {
            int isNumber = 0;
            e.Handled = !int.TryParse(e.KeyChar.ToString(), out isNumber);
        }

        private void getOfferBtn_Click(object sender, EventArgs e)
        {
            int tempVal;
            int? age = Int32.TryParse(ageInput.Text, out tempVal) ? Int32.Parse(ageInput.Text) : (int?)null;
            int? income = Int32.TryParse(incomeInput.Text, out tempVal) ? Int32.Parse(incomeInput.Text) : (int?)null;
            var student = studentCheckbox.Checked;
            MessageBox.Show(GetProduct(age, income, student));
            
        }

        private ProductsList GetProducts()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(ProductsList));

            using (FileStream fileStream = new FileStream(System.IO.Path.Combine(new System.IO.FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).DirectoryName, "Products.xml"), FileMode.Open))
            {
                return (ProductsList)serializer.Deserialize(fileStream);
            }
        }

        private BundlesList GetBundles()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(BundlesList));

            using (FileStream fileStream = new FileStream(System.IO.Path.Combine(new System.IO.FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).DirectoryName, "Bundles.xml"), FileMode.Open))
            {
                return (BundlesList)serializer.Deserialize(fileStream);
            }
        }

        public string GetProduct(int? age, int? income, bool student)
        {
            List<Result> resultList = new List<Result>();
            foreach (var product in _products.Products)
            {
                if (product.Rule.Age == null && product.Rule.Income == null && product.Rule.Student == null)
                    continue;
                bool ageCheck = false;
                bool incomeCheck = false;
                bool studentCheck = false;
                //AGE CHECK
                if (age != null)
                {
                    if (product.Rule.Age != null)
                    {
                        if (product.Rule.AgeDirection > 0)
                        {
                            if (product.Rule.Age < age)
                                ageCheck = true;
                        }
                        else
                        {
                            if (product.Rule.Age > age)
                                ageCheck = true;
                        }
                    }
                }
                else
                {
                    if (product.Rule.Age == null)
                        ageCheck = true;
                }
                //INCOME CHECK
                if (income != null)
                {
                    if (product.Rule.Income != null)
                    {
                        if (product.Rule.IncomeDirection > 0)
                        {
                            if (product.Rule.Income < income)
                                incomeCheck = true;
                        }
                        else
                        {
                            if (product.Rule.Income > income)
                                incomeCheck = true;
                        }
                    }
                }
                else
                {
                    if (product.Rule.Income == null)
                        incomeCheck = true;
                }
                //STUDENT CHECK
                if (student && (product.Rule.Student == true) || (!student && (product.Rule.Student != true)) )
                    studentCheck = true;

                if (ageCheck && incomeCheck && studentCheck)
                {
                    Result rez = new Result();
                    rez.Name = product.Name;
                    rez.Value = product.Rule.Value;
                    resultList.Add(rez);
                }
            }
            if (resultList.Count > 0)
                return "Best offer is: " + resultList.OrderByDescending(item => item.Value).First().Name;
            return "There are no suitable offers";
        }
    }
}
