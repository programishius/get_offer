﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEB_GET_OFFER
{
    [Serializable]
    public class Rule
    {
        public int? Age { get; set; }
        public int? AgeDirection { get; set; }
        public bool? Student { get; set; }
        public int? Income { set; get; }
        public int? IncomeDirection { get; set; }
        public string Products { set; get; }
        public bool? IsAccount { get; set; }
        public int Value { get; set; }
        public string BundleName { get; set; }
    }

    [Serializable]
    public class ProductsList
    {
        public List<Product> Products { get; set; }
    }
    [Serializable]
    public class Product
    {
        public string Name { set; get; }
        public Rule Rule { set; get; }
    }
    [Serializable]
    public class BundlesList
    {
        public List<Bundle> Bundles { get; set; }
    }

    [Serializable]
    public class Bundle
    {
        public string Name { get; set; }
        public List<string> Products { get; set; }
    }
    public class Result
    {
        public string Name { get; set; }
        public int Value { get; set; }
    }

}
